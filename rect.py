import pygame
import sys
from pygame.sprite import Sprite

def run_game():
	pygame.init()	
	clock = pygame.time.Clock()
	clock.tick(60)

	screen_width = 640
	screen_height = 480
	bg_color = (0,0,0)
	box_color = (132,127,114)
	DISPLAYSURF = pygame.display.set_mode((screen_width, screen_height), 0, 32)
	DISPLAYSURF.fill(bg_color)


	while True:
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_q:
					sys.exit()


		DISPLAYSURF.fill(bg_color)
		left = 100
		top = 100
		width = 100
		height = 100

		#create a rect object
		myRect = pygame.Rect(left, top, width, height)	

		# draw rect object to surface
		# pygame.draw.rect(Surface, color, rect, thickness)
		pygame.draw.rect(DISPLAYSURF, box_color, myRect, 3)

		pygame.display.update()


if __name__ == "__main__":
	run_game()

