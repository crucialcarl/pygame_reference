import pygame
import sys



def run_game():
	pygame.init()	
	clock = pygame.time.Clock()
	clock.tick(60)

	screen_width = 640
	screen_height = 480
	bg_color = (0,0,0)
	DISPLAYSURF = pygame.display.set_mode((screen_width, screen_height), 0, 32)
	DISPLAYSURF.fill(bg_color)


	while True:
		DISPLAYSURF.fill(bg_color)
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_q:
					sys.exit()
					
		pygame.display.update()


if __name__ == "__main__":
	run_game()

