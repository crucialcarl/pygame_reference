import pygame
import sys
from pygame.sprite import Sprite

class Box(Sprite):
	def __init__(self, screen, x, y, color):
		super(Box, self).__init__()
		self.screen = screen
		self.x = x
		self.y = y
		self.color = color
		self.boxSurface = pygame.Surface((32,32), pygame.SRCALPHA).convert()
		self.rect = self.boxSurface.get_rect()
		self.rect.left = self.x
		self.rect.top = self.y
		pygame.draw.rect(self.boxSurface, self.color, self.rect)
		self.screen.blit(self.boxSurface, self.rect)

	def update(self):
		self.boxSurface.fill(self.color)
		self.screen.blit(self.boxSurface, self.rect)


def run_game():
	pygame.init()	
	clock = pygame.time.Clock()
	clock.tick(60)

	screen_width = 640
	screen_height = 480
	bg_color = (255,255,255)
	box_color = (32,227,114)
	DISPLAYSURF = pygame.display.set_mode((screen_width, screen_height), 0, 32)
	DISPLAYSURF.fill(bg_color)

	boxGroup = pygame.sprite.Group()

	box = Box(DISPLAYSURF, 300, 300, box_color)
	boxGroup.add(box)

	while True:
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				if event.key == pygame.K_q:
					sys.exit()

		DISPLAYSURF.fill(bg_color)
		for i in boxGroup:
			#box.rect.top += 1
			#box.rect.left += 1
			box.update()

		pygame.display.update()


if __name__ == "__main__":
	run_game()

